import tensorflow as tf
from tensorflow.keras.layers import *
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import backend as K
from tensorflow.keras.models import Model


def gaussian_nll(y_true, y_pred):
    """Multivariate Gaussian negative loglikelihood loss function for Keras.

    Implies diagonal covariance matrix. Assumes first half of predictions are means
    and second half are log(sigma).

    Arguments:
        y_true {tensor} -- [n_samples, n_dims] ground truth values
        y_pred {tensor} -- [n_samples, n_dims*2] predicted mu and then log(sigma) values

    Returns:
        float -- negative log-likelihood averaged over samples
    """
    n_dims = int(y_pred.shape[1] / 2)
    mu = y_pred[:, :n_dims]
    logsigma = y_pred[:, n_dims:]

    mse = -0.5 * K.sum(
        K.square((y_true - mu) / (K.exp(logsigma) + K.epsilon())), axis=1
    )
    sigma_trace = -K.sum(logsigma, axis=1)
    log2pi = -0.5 * n_dims * np.log(2 * np.pi)

    log_likelihood = mse + sigma_trace + log2pi

    return K.mean(-log_likelihood)


def mse_for_nll(y_true, y_pred):
    """Mean Square Error for use with gaussian_nll (only use mu values)

    Assumes only first half of y_pred is actual prediction

    Arguments:
        y_true {tensor} -- [n_samples, n_dims] ground truth values
        y_pred {tensor} -- [n_samples, n_dims*2] predicted mu and then log(sigma) values

    Returns:
        float -- mean squared error (across samples)
    """
    n_dims = int(y_pred.shape[1] / 2)
    return K.mean(K.sum(K.square(y_pred[:, :n_dims] - y_true), axis=1))


custom_objs = {"mse_for_nll": mse_for_nll, "gaussian_nll": gaussian_nll}


def make_model(input_shape, conv_units=64, dense_units=64):
    nb_temporal_filters = conv_units
    nb_spatial_filters = conv_units
    nb_mu_dense_layers = 3
    nb_sigma_dense_layers = 3
    nb_hidden_units = dense_units
    dropout_rate = 0.5
    lrelu_alpha = 0.3

    inputs = Input(shape=input_shape)

    x_inputs_flat = Flatten()(inputs)
    x = inputs

    # Temporal Convolutions
    x = Conv2D(
        nb_temporal_filters,
        (input_shape[0], 1),
        strides=(input_shape[0], 1),
        padding="valid",
        name="temporal_fold",
    )(x)
    x = LeakyReLU(lrelu_alpha)(x)

    # Spatial Convolutions
    x = Conv2D(
        nb_spatial_filters,
        (1, input_shape[1]),
        strides=(1, 1),
        padding="valid",
        name="spatial_fold",
    )(x)
    x_spatial = LeakyReLU(lrelu_alpha)(x)

    x_spatial_flat = Flatten()(x_spatial)

    # Dense Layers
    x = Concatenate(axis=-1, name="inputs_and_tts")([x_spatial_flat, x_inputs_flat])
    for i in range(nb_mu_dense_layers):
        x = Dense(nb_hidden_units, name="mean_dense_{}".format(i))(x)
        x = LeakyReLU(lrelu_alpha)(x)
        x = Dropout(rate=dropout_rate)(x)

    z_mean = Dense(1, name="z_mean")(x)

    x = Concatenate(axis=-1, name="inputs_tts_pred")(
        [x_spatial_flat, x_inputs_flat, z_mean]
    )
    for i in range(nb_sigma_dense_layers):
        x = Dense(nb_hidden_units, name="logsigma_dense_{}".format(i))(x)
        x = LeakyReLU(lrelu_alpha)(x)
        x = Dropout(rate=dropout_rate)(x)

    z_std = Dense(1, name="z_std")(x)

    x = Concatenate(axis=-1)([z_mean, z_std])

    optimiser = Adam(learning_rate=0.0005, clipnorm=1.0)

    model = Model(inputs, x)
    model.compile(
        optimizer=optimiser, loss=gaussian_nll, metrics=[gaussian_nll, mse_for_nll]
    )
    return model


input_shape = [10, 5, 1]
model = make_model(input_shape, 32, 64)

# Convert the model.
converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()

# Save the TF Lite model.
with tf.io.gfile.GFile("adv_model.tflite", "wb") as f:
    f.write(tflite_model)
