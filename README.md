# Automated Tensorflow Lite Build and Check

Automated rebuilding of the Tensorflow Lite library. Currently builds for:
* `x86_64` - `linux`

## CMake Integration
Ships with `CMakeLists.txt` files that make `tensorflow-lite` into an `INTERFACE` library.

* Library is called `tflite`
* To include in your project
    - In top-level `CMakeLists` include `add_subdirectory("path/to/repo")`
    - In your source `CMakeLists`: `target_link_libraries(${PROJECT_NAME} PRIVATE tflite)`

In your build you will likely need to add some extra linker flags to handle `pthread` correctly by forcing use of the whole archive - this allows proper preservation of symbols:

```cmake
string(CONCAT CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} "
    "-Wl,--whole-archive -lpthread -Wl,--no-whole-archive ")
```
